var vm = new Vue({
  el: "#app",
  data: {
    name: "",
    address: "",
    hp: "",
    desc: "",
    errors: [],
    members: [],
    photoDomain: "https://demo-api-vue.sanbercloud.com",
    buttonStatus: "submit",
    idEditMember: null,
    idUploadMember: null,
  },
  methods: {
    validationForm: function () {
      this.errors = [];

      if (this.name.length < 5) {
        this.errors.push("name minimal 5");
        // this.$refs.name.focus();
      }
    },

    clearForm: function () {
      this.name = "";
      this.address = "";
      this.hp = "";
    },

    submitForm: function () {
      this.validationForm();
      if (this.errors.length === 0) {
        let formData = new FormData();

        formData.append("name", this.name);
        formData.append("address", this.address);
        formData.append("no_hp", this.hp);

        const config = {
          method: "post",
          url: "https://demo-api-vue.sanbercloud.com/api/member",
          data: formData,
        };

        axios(config)
          .then(response => {
            this.clearForm();
            this.getMember();
            alert(response.data.message);
          })
          .catch(error => {
            console.log(error);
          });
      }
    },

    getMember: function () {
      const config = {
        method: "get",
        url: "https://demo-api-vue.sanbercloud.com/api/member",
      };

      axios(config)
        .then(response => {
          this.members = response.data.members;
        })
        .catch(error => {
          console.log(error);
        });
    },

    deleteMember: function (id) {
      const config = {
        method: "post",
        url: `https://demo-api-vue.sanbercloud.com/api/member/${id}`,
        params: { _method: "DELETE" },
      };

      axios(config)
        .then(response => {
          this.getMember();
          alert(response.data.message);
        })
        .catch(error => {
          console.log(error);
        });
    },
    editMember: function (member) {
      this.name = member.name;
      this.address = member.address;
      this.hp = member.no_hp;
      this.buttonStatus = "update";
      this.idEditMember = member.id;
    },
    clearForm: function () {
      this.name = "";
      this.address = "";
      this.hp = "";
      this.buttonStatus = "submit";
      this.idEditMember = null;
      this.idUploadMember = null;
    },
    updateMember: function (id) {
      this.validationForm();
      if (this.errors.length === 0) {
        let formData = new FormData();

        formData.append("name", this.name);
        formData.append("address", this.address);
        formData.append("no_hp", this.hp);

        const config = {
          method: "post",
          url: `https://demo-api-vue.sanbercloud.com/api/member/${id}`,
          params: { _method: "PUT" },
          data: formData,
        };

        axios(config)
          .then(response => {
            this.clearForm();
            this.getMember();
            alert(response.data.message);
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    uploadFoto: function (blog) {
      this.name = blog.name;
      this.address = blog.address;
      this.hp = blog.no_hp;
      this.buttonStatus = "upload";
      this.idUploadMember = blog.id;
    },
    submitPhoto: function (id) {
      let file = this.$refs.photo.files[0];

      let formData = new FormData();
      formData.append("photo", file);

      let config = {
        method: "post",
        url: `https://demo-api-vue.sanbercloud.com/api/member/${id}/upload-photo-profile`,
        data: formData,
      };

      axios(config)
        .then(response => {
          this.clearForm();
          this.getMember();
          alert(response.data.message);
        })
        .catch(error => {
          console.log(error);
        });
    },
  },
  created() {
    this.getMember();
  },
});
